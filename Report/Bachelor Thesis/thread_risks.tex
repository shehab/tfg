\chapter{Risks Associated with Multi-Threaded Programming}
On modern machines multi-threaded programming has become a necessity in order to make use of the systems using multiple cores. Multi-threaded programming is also very important for modern applications that need to segment their work in order to be able to do it more efficiently. 

With the benefits of multi-threaded programming however come several risks. The increase of efficiency causes an increase in the complexity of the work, which leads to many new possible errors. These errors are more tricky to detect, seeing as they do not necessarily effect the results of the program. Using multi-threaded programming makes the outcome of the program non-deterministic, unlike the deterministic nature of purely sequential programs.

There are two main classes of errors that can occur in multi-threaded programming: Race conditions and Deadlocks. There are many variations of these errors, but any error related with the multi-threaded nature of a program can be categorized into one of those two error classes.

\section{Race Conditions}
Race conditions in multi-threaded programming causes the non-deterministic nature of multi-threaded programming. Race conditions occur when two different threads try to access the same data without sufficient synchronization. While one thread reads the other writes, and since there is no synchronization between them, the read sometimes occurs before, and sometimes after the write. This is what causes the non-deterministic nature of the results of the program.

There are many kinds of race conditions, but they can be divided into 10 main subcategories.

\subsection{Simple Race}

Simple races are situations in which a variable of a built-in type is being accessed by two different threads (once as a read, and once as a write).

\begin{table}[htb]
\label{tab:simple_race}
\begin{center}
\begin{tabular}{|l|l|}								
\hline
\textit{Thread 1}	& \textit{Thread 2}	\\	\hline
int a;				& a++;				\\
a++;				& 					\\	
\hline

\end{tabular}
\caption{Simple Race}
\end{center}
\end{table}


\subsection{Race on a Complex Type}

This situation is similar to the simple race, except that it occurs on complex objects. If they are created and handled without synchronization, they create a race which is more dangerous than simple races.

\begin{table}[htb]
\label{tab:race_on_complex_type}
\begin{center}
\begin{tabular}{|l|l|}								
\hline
\textit{Thread 1}			& \textit{Thread 2}		\\	\hline
std::vector<int> vect(20);	& vect[456] = 0;		\\
vect[123] = 1;				&  						\\	
\hline

\end{tabular}
\caption{Race on a Complex Type}
\end{center}
\end{table}

\subsection{Notification Race}

This kind of race conditions occurs, when using a boolean flag as a communication method between the threads, without using any synchronization methods.

\begin{table}[htb]
\label{tab:notification_race}
\begin{center}
\begin{tabular}{|l|l|}
							
\hline
\textit{Thread 1}			& \textit{Thread 2}		\\	\hline
bool ready = false;			& 						\\
while(!ready)				& do\_something()		\\	
\quad do\_something()		& ready = true;			\\
\hline

\end{tabular}
\caption{Notification Race}
\end{center}
\end{table}

\subsection{Publishing Objects without Synchronization}

If one thread were to initialize an object that was originally initialized to null by the main thread, while another thread is spins waiting for the object to have a non-null value, without proper synchronization the compiler could do transformations (code motion) which could lead to failures. This can also cause cache-related errors in certain architecture.

\begin{table}[htb]
\label{tab:obj_publishing_race}
\begin{center}
\begin{tabular}{|l|l|l|}
							
\hline
\textit{Main Thread}	&	\textit{Thread 1}		& \textit{Thread 2}		\\	\hline
						&							& while(obj == NULL)	\\
Obj* obj = NULL;		&	obj = new 				& \quad yield;			\\	
						&							& obj->do\_something;	\\
\hline

\end{tabular}
\caption{Object Publishing Race}
\end{center}
\end{table} 

\subsection{Initializing Objects without Synchronization}

Initialization of an object without correct synchronization can cause memory leaks, as an object might be constructed twice.

\begin{table}[htb]
\label{tab:init_obj_w/o_synch}
\begin{center}
\begin{tabular}{|l|l|}	

static Obj* obj = NULL;	
void InitObj()\{								\\
\quad if(!obj)									\\
\qquad obj = new Obj							\\
\}												\\
												\\
												\\
												\\	\hline
\textit{Thread 1}		& \textit{Thread 2}		\\	\hline
InitObj();				& InitObj();			\\
\hline

\end{tabular}
\caption{Object Initialization Race}
\end{center}
\end{table}

\subsection{Write during a ReaderLock}

Misusing a ReaderLock, and writing under it, can cause data races, as ReaderLocks only synchronize between readers and writers, and do not block several threads from reading at the same time.

\begin{table}[htb]
\label{tab:reader_lock_write}
\begin{center}
\begin{tabular}{|l|l|}
							
\hline
\textit{Thread 1}			& \textit{Thread 2}		\\	\hline
mutex.ReaderLock();			& mutex.ReaderLock()	\\
var = 1;					& var = 2;				\\	
mutex.ReaderUnlock();		& mutex.ReaderUnlock();	\\
\hline

\end{tabular}
\caption{Writing with ReaderLock}
\end{center}
\end{table}

\subsection{Adjacent Bit Fields}

When using bitfields it is possible to write code, that seems correct, but due to the padding involved with making bitfields it is possible to create a race condition when updating the fields without proper synchronization.

\begin{table}[htb]
\label{tab:adj_bit_fields_race}
\begin{center}
\begin{tabular}{|l|l|}	

struct x\{									\\
\quad int a:4, b:4;							\\
}											\\
											\\
											\\	\hline
\textit{Thread 1}	& \textit{Thread 2}		\\	\hline
x.a++;				& x.b++;				\\
\hline

\end{tabular}
\caption{Adjacent Bit Fields Race}
\end{center}
\end{table}

\subsection{Race During Destruction}

If a thread initializes an object, and passes it to another thread through the stack, the object is destroyed in its scope, before the second thread has finished.

\begin{table}[htb]
\label{tab:obj_publishing_race}
\begin{center}
\begin{tabular}{|l|l|}	

\hline
\textit{Thread 1}				& \textit{Thread 2}		\\	\hline
void Thread1(){					& 						\\
\quad Object obj;				& do_something(&obj);	\\
\quad ExecuteCallbackInThread2(	& 						\\
\qquad SomeCallBack, &obj)		& 						\\
}								&						\\
\hline

\end{tabular}
\caption{Object Publishing Race}
\end{center}
\end{table}

\subsection{Race on vptr}

\section{Deadlock}

A deadlock in the context of multi-threaded programming is a situation in which one or more threads are blocked because of the 