\contentsline {chapter}{\numberline {1.}Introduction}{8}
\contentsline {chapter}{\numberline {2.}System Specifications}{9}
\contentsline {section}{\numberline {2.1}Processor}{9}
\contentsline {section}{\numberline {2.2}RAM}{9}
\contentsline {chapter}{\numberline {3.}Project Goals}{10}
\contentsline {chapter}{\numberline {4.}Risks Associated with Multi-Threaded Programming}{11}
\contentsline {section}{\numberline {4.1}Race Conditions}{11}
\contentsline {subsection}{\numberline {4.1.1}Simple Race}{12}
\contentsline {subsection}{\numberline {4.1.2}Race on a Complex Type}{12}
\contentsline {subsection}{\numberline {4.1.3}Notification Race}{12}
\contentsline {subsection}{\numberline {4.1.4}Publishing Objects without Synchronization}{13}
\contentsline {subsection}{\numberline {4.1.5}Initializing Objects without Synchronization}{13}
\contentsline {subsection}{\numberline {4.1.6}Write during a ReaderLock}{13}
\contentsline {subsection}{\numberline {4.1.7}Adjacent Bit Fields}{14}
\contentsline {subsection}{\numberline {4.1.8}Double-Checked Locking}{14}
\contentsline {subsection}{\numberline {4.1.9}Race During Destruction}{14}
\contentsline {subsection}{\numberline {4.1.10}Race on vptr}{14}
\contentsline {section}{\numberline {4.2}Deadlock}{14}
\contentsline {chapter}{\numberline {5.}Tools Being Used}{15}
\contentsline {section}{\numberline {5.1}Valgrind's Helgrind}{15}
\contentsline {section}{\numberline {5.2}Valgrind's DRD}{15}
\contentsline {section}{\numberline {5.3}LLVM's ThreadSanitizer}{15}
\contentsline {section}{\numberline {5.4}GNU's ThreadSanitizer}{15}
\contentsline {chapter}{\numberline {6.}Unit Tests}{16}
\contentsline {section}{\numberline {6.1}Helgrind Tests}{16}
\contentsline {section}{\numberline {6.2}DRD Tests}{25}
