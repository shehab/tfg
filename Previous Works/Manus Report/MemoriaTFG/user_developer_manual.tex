
\chapter{User and developer manual}
This manual assumes that Flex-MPI has been installed. This manual explains how to compile, link and run Flex-MPI applications. 

\section{Compilation and linking}
In order to compile your Flex-MPI application the following sample Makefile can be used. It has to be noted the the paths of \texttt{LIBEMPI\_DIR, PAPI\_DIR, GLPK\_DIR} and \texttt{MPICC} are to be substituted.

\texttt{
LIBEMPI\_DIR=/FLEX\_MPI/INSTALL/DIRECTORY\\    
    PAPI\_DIR=/PAPI/INSTALL/DIRECTORY\\
    GLPK\_DIR=/GLPK/INSTALL/DIRECTORY\\
    MPICC=/MPICC/DIRECTORY\\
\\
    PAPI=-I\$(PAPI\_DIR)/include/ -L\$(PAPI\_DIR)/lib -lpapi -L\$(PAPI\_DIR)/lib -lpfm\\
    GLPK=-I\$(GLPK\_DIR)/include/ -L\$(GLPK\_DIR)/lib/ -lglpk\\
    EMPI=-I\$(LIBEMPI\_DIR)/include/ -L\$(LIBEMPI\_DIR)/lib/ -lempi\\
    CFLAGS=-Wall 
    \\
    example:\\
        \$(MPICC) \$(CFLAGS) -o example example.c \$(EMPI) \$(PAPI) \$(GLPK)\\
        @echo "+++MAKE example COMPLETE+++"\\
}\\

After having prepared a suitable Makefile, run the command \texttt{make example} under the appropriate directory. 

\section{Execution}

It has to be noted that the execution scheme varies among the malleability policy used. Normal policies are considered to be those who do not require to use any monitoring capabilities (\textit{i.e }\texttt{-policy-efficiency, -policy-cost-irregular,\hfill \break -policy-efficiency-irregular, -policy-cost, -policy-malleability,\hfill \break -policy-malleability-conditional} and \texttt{-policy-lbalance} options).

\subsection{Normal malleability policies}
In order to execute a Flex-MPI application the follwoing command is used:\\
\texttt{./executable [executable args] [Flex-MPI global opts]}\\
The following options may account for \texttt{[Flex-MPI global opts]}:
\begin{itemize}
\item \textbf{Global options}
	\begin{itemize}
	\item \texttt{-cfile \{name\}} File containing host names and cores per host
	\end{itemize}
\item \textbf{Dynamic processes options} (none by default)
    \begin{itemize}
    \item \texttt{-policy-efficiency \{sec\} \{thrsld\}}  Set efficiency policy and the performance constraint \{time in seconds\} \{threshold in percentage\}
    \item \texttt{-policy-efficiency-irregular \{perc\} \{thrsld\}}  Set efficiency policy for irregular application and the performance constraint \{performance improvement in percentage\} \{threshold in percentage\}
    \item \texttt{-policy-cost \{sec\} \{thrsld\}}  Set cost policy and the performance constraint \{time in seconds\} \{threshold in percentage\}
    \item \texttt{-policy-cost-irregular \{perc\} \{thrsld\}}  Set cost policy for irregular application and the performance constraint \{performance improvement in percentage\} \{threshold in percentage\}
    \item \texttt{-policy-energy \{sec\} \{thrsld\}}  Set energy policy and the performance constraint \{time in seconds\} \{threshold in percentage\}
	\item \texttt{-policy-guided -gfile \{gfile location\}}  Set guided policy    
    \item \texttt{-policy-malleability}  Set malleability policy
    \item \texttt{-policy-malleability-conditional}  Set conditional malleability policy
    \item \texttt{-policy-lbalance}  Set load balancing policy
    \end{itemize}
\item \textbf{Load balancing policy options}
    \begin{itemize}
    \item \texttt{-lbpolicy-mflops}  Set MFLOP/S policy (default)
    \item \texttt{-lbpolicy-counts}  Set COUNT/S policy
    \item \texttt{-lbpolicy-disabled} Disable load balancing
    \end{itemize}
\item \textbf{Adaptability policy options}
    \begin{itemize}
    \item \texttt{-apolicy-exhaustive}  Set exhaustive estimation policy (default)
    \item \texttt{-apolicy-lp}  Set linear programming policy
    \end{itemize}
\item \textbf{Spawning policy options}
    \begin{itemize}
    \item \texttt{-spolicy-available}  Set available nodes policy
    \item \texttt{-spolicy-occupied}  Set occupied nodes policy (default)
    \end{itemize}
\item \textbf{Global data allocation options}
    \begin{itemize}
    \item \texttt{-galloc}  Set global data allocation (rows, nnz, or fcost)
    \end{itemize}
\item \textbf{Load balancing threshold options}
    \begin{itemize}
    \item \texttt{-threshold}  Set threshold in miliseconds (default \{0.15 per cent\})
    \item \texttt{-self-adaptation}  Set self adaptation when threshold is reached (default \{no\})
    \item \texttt{-hsteps}  Set number of steps for historical monitoring (default \{1\})
    \end{itemize}
\item \textbf{Monitor options}
    \begin{itemize}
    \item \texttt{-ni \{value\}}  Number of iterations of the sampling interval (default \{10\})
    \item \texttt{-nilb \{value\}}  Number of iterations of the sampling interval for load balancing in irregular applications (default \{10\})
    \end{itemize}
\item \textbf{Examples}
    \begin{itemize}
    \item \texttt{mpiexec -np 10 -machinefile myhostfile ./myprogram -cfile \hfill \break mycorefile -policy-efficiency 3000 -ni 100}
    \item \texttt{mpiexec -np 10 -machinefile myhostfile ./myprogram -cfile \hfill \break mycorefile -policy-lbalance -lbpolicy-mflops -ni 300}
    \end{itemize}
\end{itemize}

\subsection{Energy-aware policies}
It has to be made clear that in order to perform the instructions presented in the current section, \textbf{root} user privileges are required. Moreover, it has to be mentioned that the presented explanations are performed with the those example applications present under the \texttt{examples} directory of Flex-MPI. \\

For those policies that use energy monitoring capabilities (\textit{i.e.} \texttt{-policy-energy, -policy-energy-efficiency}, an additional procedure has to be followed after the compilation process, due to the nature of the RAPL interface, used to measure energy consumption. Since  the PAPI RAPL component relies on reading the RAPL MSR registers directly from user space\cite{ref:papi_rapl}, by means of the MSR kernel module, its usage has to be enabled:\\
\begin{enumerate}
\item \texttt{sudo cat /boot/config-<kernel-version> \textpipe} \texttt{grep MSR}  Verify that your Linux kernel has the MSR module compiled (output options should be y, m, m)
\item \texttt{sudo modprobe msr}  Load the msr module
\item \texttt{sudo chmod 666 /dev/cpu/*/msr}  Change the MSR module's permissions

Another particularity is that executables willing to open the MSR device file, require since Linux kernel 3.7 to have the \texttt{CAP\_SYS\_RAWIO} capability \cite{rapl_2}. Note that one needs superuser privileges to grant the \texttt{RAWIO} capability to an executable, and that the executable cannot be located on a shared network file system  partition. Thus we will systematically copy the executable onto a local directory, for example \texttt{/tmp}. Thus, in order to do so:
\item \texttt{sudo chmod 0777 /tmp}  Granting executable permissions to the \texttt{/tmp} directory. 
\item \texttt{cp /path-to-lib/libempi/examples/<executable\_name> /tmp}  Copying executable to the \texttt{/tmp} directory.
\item \texttt{sudo /sbin/setcap cap\_sys\_rawio=ep /tmp/<executable\_name>}   Setting \hfill \break\texttt{RAWIO} capabilities to executable. 

Additionally, the dynamic linker will remove variables that control dynamic linking from the environment of executables with extended rights, such as executables with raised capabilities. It is the case of the \texttt{LD\_LIBRARY\_PATH} environment variable. Therefore, executables that have the \texttt{RAWIO} capability can only load shared libraries from default system directories. Thus, in the case a network file system is being used, symbolic links have to be created from the default system directory, \texttt{/usr/lib/}:\\

\item \texttt{cd /usr/lib} 
\item \texttt{sudo ln -s /path/to/libempi/lib/libempi.so }  Create a symbolic link for Flex-MPI in the \texttt{/usr/lib }directory. 
\item \texttt{sudo ln -s /path/to/libs/papi/lib/libpapi.so.5.3.2.0}  Create a symbolic link for th PAPI library in the \texttt{/usr/lib }directory. 
\item \texttt{sudo ln -s /path/to/libs/glpk/lib/libglpk.so.0}  Create a symbolic link for the GLPK library in the \texttt{/usr/lib }directory. 

After having accomplished the aforementioned procedure, we can proceed to run the executables:\\

\item \texttt{-policy-energy \{sec\} \{thrsld\}}  Set energy policy and the performance constraint \{time in seconds\} \{threshold in percentage\}
\item \texttt{-policy-energy-efficiency \{sec\} \{thrsld\}}  Set energy-efficiency policy and the performance constraint \{time in seconds\} \{threshold in percentage\}
\end{enumerate}