\select@language {british}
\contentsline {chapter}{}{v}{chapter*.1}
\contentsline {chapter}{Agradecimientos}{vii}{chapter*.2}
\contentsline {chapter}{Abstract}{xi}{chapter*.3}
\contentsline {chapter}{Keywords}{xiii}{chapter*.4}
\contentsline {chapter}{Resumen}{xv}{chapter*.5}
\contentsline {chapter}{Palabras Clave}{xvii}{chapter*.6}
\contentsline {chapter}{List of Figures}{xxiii}{dummy.8}
\contentsline {chapter}{List of Tables}{xxv}{dummy.10}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.12}
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.13}
\contentsline {section}{\numberline {1.2}Research group context}{5}{section.17}
\contentsline {section}{\numberline {1.3}Project objectives}{5}{section.18}
\contentsline {subsection}{\numberline {1.3.1}Main objectives}{5}{subsection.19}
\contentsline {subsection}{\numberline {1.3.2}Secondary objectives}{6}{subsection.20}
\contentsline {section}{\numberline {1.4}Document structure}{6}{section.21}
\contentsline {chapter}{\numberline {2}State of the art}{9}{chapter.22}
\contentsline {section}{\numberline {2.1}High performance computing}{9}{section.23}
\contentsline {subsection}{\numberline {2.1.1}Energy in supercomputers}{10}{subsection.24}
\contentsline {section}{\numberline {2.2}Recent supercomputer challenges: exascale}{12}{section.25}
\contentsline {subsection}{\numberline {2.2.1}Adapting MPI to Exascale}{12}{subsection.26}
\contentsline {section}{\numberline {2.3}MPI: support for malleability}{13}{section.27}
\contentsline {section}{\numberline {2.4}Recent work on malleability in MPI applications}{14}{section.28}
\contentsline {section}{\numberline {2.5}Recent work on energy in parallel applications}{15}{section.32}
\contentsline {subsection}{\numberline {2.5.1}RAPL}{17}{subsection.36}
\contentsline {section}{\numberline {2.6}Summary}{18}{section.37}
\contentsline {chapter}{\numberline {3}Development framework}{21}{chapter.38}
\contentsline {section}{\numberline {3.1}Software alternatives for developing parallel applications}{21}{section.39}
\contentsline {subsection}{\numberline {3.1.1}OpenMP and MPI}{21}{subsection.40}
\contentsline {subsection}{\numberline {3.1.2}Different MPI versions}{24}{subsection.43}
\contentsline {section}{\numberline {3.2}Energy measurement alternatives}{26}{section.44}
\contentsline {subsection}{\numberline {3.2.1}External probes}{26}{subsection.45}
\contentsline {subsection}{\numberline {3.2.2}Models and tools}{26}{subsection.46}
\contentsline {subsection}{\numberline {3.2.3}Hardware counters}{27}{subsection.47}
\contentsline {section}{\numberline {3.3}Design alternatives}{28}{section.48}
\contentsline {subsection}{\numberline {3.3.1}Processor locality based on cache usage}{28}{subsection.49}
\contentsline {subsection}{\numberline {3.3.2}Improvement of the optimization algorithm}{31}{subsection.53}
\contentsline {subsection}{\numberline {3.3.3}Energy-aware functionality}{32}{subsection.57}
\contentsline {section}{\numberline {3.4}Development framework}{33}{section.58}
\contentsline {subsection}{\numberline {3.4.1}Operating System}{33}{subsection.59}
\contentsline {subsection}{\numberline {3.4.2}Programming languages}{34}{subsection.61}
\contentsline {subsection}{\numberline {3.4.3}External libraries}{35}{subsection.62}
\contentsline {subsubsection}{\numberline {3.4.3.1}MPICH}{35}{subsubsection.63}
\contentsline {subsubsection}{\numberline {3.4.3.2}PAPI}{36}{subsubsection.64}
\contentsline {subsubsection}{\numberline {3.4.3.3}GLPK}{39}{subsubsection.67}
\contentsline {subsection}{\numberline {3.4.4}Development software}{40}{subsection.69}
\contentsline {subsection}{\numberline {3.4.5}Hardware Platform}{42}{subsection.71}
\contentsline {chapter}{\numberline {4}Project description}{45}{chapter.73}
\contentsline {section}{\numberline {4.1}Requirements elicitation}{45}{section.74}
\contentsline {section}{\numberline {4.2}Requirements Specification}{46}{section.75}
\contentsline {subsection}{\numberline {4.2.1}User requirements}{48}{subsection.77}
\contentsline {subsection}{\numberline {4.2.2}Functional Requirements}{50}{subsection.86}
\contentsline {subsection}{\numberline {4.2.3}Non-Functional Requirements}{58}{subsection.123}
\contentsline {section}{\numberline {4.3}Use case}{61}{section.133}
\contentsline {section}{\numberline {4.4}Validation tests}{61}{section.135}
\contentsline {subsection}{\numberline {4.4.1}Validation tests}{62}{subsection.137}
\contentsline {section}{\numberline {4.5}Traceability matrix}{74}{section.202}
\contentsline {section}{\numberline {4.6}Feasibility study}{74}{section.204}
\contentsline {subsection}{\numberline {4.6.1}Technical constraints}{74}{subsection.205}
\contentsline {subsection}{\numberline {4.6.2}Sociocultural constraints}{76}{subsection.206}
\contentsline {subsection}{\numberline {4.6.3}Economic constraints}{77}{subsection.207}
\contentsline {subsection}{\numberline {4.6.4}Legal constraints}{78}{subsection.208}
\contentsline {section}{\numberline {4.7}Project planning}{78}{section.209}
\contentsline {subsection}{\numberline {4.7.1}Development phases}{78}{subsection.210}
\contentsline {subsection}{\numberline {4.7.2}Temporal planning}{80}{subsection.216}
\contentsline {subsection}{\numberline {4.7.3}Costs planning}{83}{subsection.221}
\contentsline {chapter}{\numberline {5}Proposal}{87}{chapter.227}
\contentsline {section}{\numberline {5.1}Flex-MPI overview}{87}{section.228}
\contentsline {subsection}{\numberline {5.1.1}Application internal flow}{93}{subsection.231}
\contentsline {section}{\numberline {5.2}Guided policy}{94}{section.241}
\contentsline {section}{\numberline {5.3}Energy-aware malleability policies}{98}{section.257}
\contentsline {subsection}{\numberline {5.3.1}Energy minimization policy}{99}{subsection.258}
\contentsline {subsection}{\numberline {5.3.2}Energy-efficiency maximization policy}{99}{subsection.259}
\contentsline {section}{\numberline {5.4}Models}{101}{section.262}
\contentsline {subsection}{\numberline {5.4.1}Energy prediction model}{101}{subsection.263}
\contentsline {subsection}{\numberline {5.4.2}Energy-efficiency prediction model}{105}{subsection.283}
\contentsline {section}{\numberline {5.5}Algorithms}{107}{section.307}
\contentsline {subsection}{\numberline {5.5.1}Energy-minimization policy}{107}{subsection.308}
\contentsline {subsection}{\numberline {5.5.2}Energy-efficiency maximization policy}{112}{subsection.358}
\contentsline {section}{\numberline {5.6}Linear-programming problems}{112}{section.360}
\contentsline {subsection}{\numberline {5.6.1}Energy-minimization policy}{115}{subsection.361}
\contentsline {subsection}{\numberline {5.6.2}Performance-per-watt maximization policy}{116}{subsection.363}
\contentsline {section}{\numberline {5.7}Summary}{116}{section.365}
\contentsline {chapter}{\numberline {6}Evaluation}{119}{chapter.366}
\contentsline {section}{\numberline {6.1}Validation}{119}{section.367}
\contentsline {subsection}{\numberline {6.1.1}Validation results}{120}{subsection.369}
\contentsline {section}{\numberline {6.2}Hardware platform description and evaluation}{124}{section.389}
\contentsline {subsection}{\numberline {6.2.1}Network model}{125}{subsection.391}
\contentsline {subsection}{\numberline {6.2.2}Turboboost}{126}{subsection.395}
\contentsline {section}{\numberline {6.3}Considered benchmarks}{128}{section.397}
\contentsline {subsection}{\numberline {6.3.1}Jacobi}{128}{subsection.398}
\contentsline {subsection}{\numberline {6.3.2}Conjugate Gradient}{130}{subsection.401}
\contentsline {section}{\numberline {6.4}Experimental results}{131}{section.404}
\contentsline {subsection}{\numberline {6.4.1}Energy minimization policy}{132}{subsection.406}
\contentsline {subsection}{\numberline {6.4.2}Performance-per-watt maximization policy}{136}{subsection.412}
\contentsline {chapter}{\numberline {7}Conclusions and future work}{143}{chapter.419}
\contentsline {section}{\numberline {7.1}Conclusions}{143}{section.420}
\contentsline {section}{\numberline {7.2}Future work}{145}{section.421}
\contentsline {chapter}{\numberline {A}Acronyms}{147}{appendix.422}
\contentsline {chapter}{\numberline {B}Installation manual}{149}{appendix.423}
\contentsline {section}{\numberline {B.1}MPICH}{149}{section.424}
\contentsline {section}{\numberline {B.2}GLPK}{150}{section.437}
\contentsline {section}{\numberline {B.3}PAPI}{150}{section.448}
\contentsline {section}{\numberline {B.4}FLEX-MPI}{151}{section.459}
\contentsline {chapter}{\numberline {C}User and developer manual}{153}{appendix.467}
\contentsline {section}{\numberline {C.1}Compilation and linking}{153}{section.468}
\contentsline {section}{\numberline {C.2}Execution}{154}{section.469}
\contentsline {subsection}{\numberline {C.2.1}Normal malleability policies}{154}{subsection.470}
\contentsline {subsection}{\numberline {C.2.2}Energy-aware policies}{156}{subsection.471}
\contentsline {chapter}{Bibliography}{159}{dummy.484}
