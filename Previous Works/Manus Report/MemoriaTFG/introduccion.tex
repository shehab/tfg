\chapter{Introduction}
Information technology infrastructure's energy consumption grows at a very fast pace. Every decade, this energy consumption is doubled, growing at a 7\% yearly rate. As a matter of fact, IT infrastructure's energy consumption in 2012 was of 900TWh, which corresponds to 4.6\% of the world's electrical energy production \cite{VanHeddeghem:2014:TWI:2657027.2657141}, \cite{3234355}. Thus, with expected proliferation of IT for the following years, energy consumption starts growing attention. 

This chapter serves as an introduction to the Final Degree Project. Not only the motivation and project objectives are described, but also the context of the student as well as the whole document's structure are described too. Particularly speaking, Section \ref{S:motivation} presents the projects motivation, outlining the main challenges that this project is facing. Section \ref{S:research_group_context} presents the context of the student within the ARCOS research group, which has lead him to contribute to the Flex-MPI library. Finally sections \ref{S:project_objectives} and \ref{S:document structure} present the main and secondary objectives of the project and the document's roadmap, respectively. 

\section{Motivation}
\label{S:motivation}
%http://insidehpc.com/hpc-basic-training/what-is-hpc/
High Performance Computing most generally refers to the practice of aggregating computing power in a way that delivers much higher performance than one could get out of a typical desktop computer or workstation in order to solve large problems in science, engineering, or business \cite{insidehpc}.

This performance enhancement is achieved due to application-level parallelism, or in other words, the simultaneous execution of this application in multiple computing nodes. It turns out that this performance enhancement comes hand in hand with the concept of cost, associated to the resources used by this parallel deployment. As a matter of fact, we encounter at this point an unavoidable trade-off between cost and performance, which leads to the concept of efficiency. We have to bear in mind that these parallel applications have a notable impact on the system's energy consumption, which has in and of itself a considerable footprint of the overall system's cost.

Obtaining the best performance, due to their commercial or research purpose, is crucial for compute infrastructures where it is necessary to pay for their use and hence in order to make the most out of them, minimizing cost and maximizing performance are conditions \textit{sine qua non}. 

In large computing facilities, energy represents an important burden in the overall operational cost and its reduction is one of the major challenges in the datacentre design. Nowadays, in state-of-the-art facilities like Google, over 87\% of power is consumed by the servers themselves~\cite{Google}. However, when we consider the trends in the design of future platforms like Exascale systems~\cite{balaji-igcc13-green500}, it would require 20-fold improvement in energy efficiency to make them feasible. For this reason, power consumption is still a critical limiting factor. The existing approaches to reduce the power include solutions ranging from the ASIC\footnote{An application-specific integrated circuit (ASIC) is an integrated circuit tuned for a particular use, instead of being intended for general-purpose use. } and architecture design levels to the software level. In the last years, heterogeneous platforms have become one of the most energy-efficient alternatives to HPC where the execution of parallel applications is simultaneously performed in CPUs and accelerators like GPGPUs, Xeon Phi, and FPGAs\footnote{A field-programmable gate array is an integrated circuit planned so to be configured by the end-user after manufacturing, making it "field-programmable". The FPGA configuration is generally specified using a hardware description language (HDL), similar to that used for ASICs.}. In these platforms, the application performance and energy consumption can dramatically vary according the compute node class that is being used. Figure \ref{F:budget}~\cite{ref:geenen} shows the budget breakdown in academic clusters. We can appreciate  that electricity accounts for 28\% of the overall cost, competing with the cost of the servers themselves. 
\begin{figure}[t]
\centering
\includegraphics{./figuras/buget_power2}
\caption{HPC system budget breakdown in academics~\cite{ref:geenen}.}
\label{F:budget}
\end{figure}


HiPEAC, the European Network of Excellence on High Performance and Embedded Architecture and Compilation, is an institution that aims to encourage innovation and collaboration as well as coordinating research while maintaining computational growth. 

Its goal is to \textit{steer and increase the European research in the area of high-performance and embedded computing systems, and stimulate cooperation between a) academia and industry and b) computer architects and tool builders}~\cite{ref:hipeac}.

This institution releases the \textit{Vision} document, where the latest contributions to the state-of-the-art are visited, as well as numerous key challenges that tackle the latest problems are presented. Power and energy efficiency are one of the key challenges presented in the document, where, textually speaking, it is claimed that \textit{We need to overcome energy as the limiting factor for increasing system performance}~\cite{3234355}.

Within this document numerous challenges are presented. Among its primary challenges (section 2.1) we can find \textit{Energy Efficiency}. Therein, energy is presented as being the limiting factor for performance, binding any further performance increase with energy consumption reduction. Among the proposed solutions we find two that cover this projects scope:
\begin{itemize}
\item\textit{Develop formalisms, methodologies and tools for adequate precision computing, or more generally to deal with desired level of Quality of Service: tools that take into account power, security, time, which use the design-by-contract paradigm, which can reduce over-specifications, and which can work both on predictable and on reactive systems.}
\item \textit{Ensure interoperability not only across systems, but also enable cross layer information flows within and between virtual systems and APIs. The resulting holistic overview will enable global system and cross-system level optimisation schemes, especially for managing energy.}
\end{itemize}

Additionally, HiPEAC remarks that optimization should not only apply to computing, but also to communication. Thus, taking into account this setting, we find the approach followed in this document to fully  fit with HiPEAC's proposal.

The Defense Advanced Research Projects Agency (DARPA) targets a 20MW exaflop ($10^{18}$ FLOPS) supercomputer~\cite{citeulike:10817006}. This has brought energy-efficiency to the vanguard of HPC. However, this exaflop system implies a 56.8-fold performance improvement with only a 2.4-fold increase in power consumption~\cite{balaji-igcc13-green500}. When facing this scenario we notice that energy consumption reduction has become a critical research area, as well as performance's most limiting factor. 

HPC exhibits two major classes of users: technical and enterprise customers. The former comprises scientists, engineers and researchers while later category encompasses the corporate data centres~\cite{ref:insidehpc3}. Very likely, differences in the scale of the cluster these two clients make exhibit will exist. Though it is true that in large computing facilities the potential benefits of our approach are larger, no matter the cluster's size proportional benefits will be achieved. 

This work faces the problem of running parallel MPI applications on heterogeneous platforms in an efficient way, from both energy and performance perspectives. We consider two different energy metrics: the overall CPU energy consumed by the application and the achieved performance per watt (measured in FLOPS or MIPS per watt). The proposed solution addresses the previous problem from a different perspective: given a performance requirement and an energy metric, our approach finds the most appropriate number of processes and the process-compute-node mapping that meets both requirements. 

Coming back to the concept of efficiency, and due to the fact that the operational cost of these hardware environments is strictly related to the amount of energy that is consumed, we believe that trying to enhance this energy consumption is definitively very convenient. 

Thus, being energy-efficiency a cutting-edge topic in HPC, this Bachelor Thesis is headed to add a energy-aware functionality to an already existing a tool, that given real-time monitoring data of the energy consumption of a given application will reduce this energy consumption in different ways, according to the user's goal (\textit{i.e.} minimize the energy consumption given a performance constraint, maximize the energy efficiency given a performance constraint and maximize this energy efficiency with no performance constraint). 
%las referencias a art�culos se ponen con \cite, 
%las referencias a im�genes \ref, 
%y las referencias a ecuaciones \eqref

\section{Research group context}
\label{S:research_group_context}
Due to the scope of the project, we consider that it is worth mentioning the situation of the student in question. Since September 2014, the author has collaborated in the ARCOS research group through a \textit{Beca de Colaboraci�n}, funded by the Spanish \textit{Ministerio de Educaci�n}. Subsequently and through this grant, the student has been able to participate in the development of the Flex-MPI library, and thus perform this Bachelor Thesis.

The grand objective is to develop a lab assignment for the subject \textit{Desarrollo de software de sistemas} focused on HW counters and the effect of HW events on application performance. The fulfilment of this project has allowed the student to get an insight into hardware counters and gather sufficient knowledge to participate in the development of the Flex-MPI library. 

With regard to the ARCOS group, its main research activities are oriented to HPC and parallel I/O systems, as well as distributed and real-time applications. Particularly speaking optimization of I/O operations, heterogeneous distributed computing (OpenCL, CUDA), cloud computing, lock-free programming and code refactorization for heterogeneous architectures. 

\section{Project objectives}
\label{S:project_objectives}
Having both the motivation an the research group context established, the project objectives, both the main ones, along with secondary objectives are presented hereinafter. 

\subsection{Main objectives}
Flex-MPI is an extension of the MPI library able of monitoring, predicting the application performance, balancing and distributing the workload and reconfigure the application by changing the number of processes on which it runs, both automatically and in runtime.

Besides getting familiarised and understand the internals of the Flex-MPI library, the main objective of this bachelor thesis is to grant energy-awareness to Flex-MPI, according to the user preferences. Thus the main objectives of the project can be presented as:
\begin{itemize}
\item \textbf{Deployment and analysis of Flex-MPI.} Since the underlying platform has changed from the moment where Flex-MPI was initially developed, its deployment is far from being trivial, since the original code requires to be precisely modified so as to be correctly deployed. In order to perform this modification, a familiarisation process must take place and Flex-MPI has to be tuned for the new platform. . 
\item \textbf{Addition of new functionalities to Flex-MPI.} The main goal of this research project is to extend the malleability capabilities of the original library. Particularly, we aim to introduce three new policies, with the following functionalities: 
	\begin{itemize}
	\item \textbf{Minimize energy.} Given a user performance objective, the energy consumption of the application is minimized, and the performance objective satisfied. 
	\item \textbf{Maximize energy-efficiency.} Given a user performance objective, the energy efficiency (expressed in performance-per-watt) of the application is maximized, satisfying as well the performance objective.
	\item \textbf{Guided reconfigurations.} Allows to the user to set reconfigurations and create an energy and performance application-specific profile. 
	\end{itemize}
\item \textbf{Evaluation of the new functionalities.} The contributions of this research project shall be thoroughly evaluated, using different benchmarks, in order to asses its effect over the overall energy consumption or performance-per-watt. 
\end{itemize}

\subsection{Secondary objectives}
\begin{itemize}
\item \textbf{Usable by third parties:} It is desired that the research and educational community can use Flex-MPI. In order to do so, an installation manual and a usage manual will be provided along with the application. 
\item \textbf{Complete backwards compatibility with Flex-MPI:} The addition of new functionality will respect the already existing ones, and will be perfectly integrated with the tool, respecting the output and input format. 
%\item \textbf{Architecture independent:} Despite the fact that the energy-awareness of the tool is dependent on each platform, its development must be carried out in a way that supports any node class.
\end{itemize} 

\section{Document structure}
\label{S:document structure}
In this section the structure of the document is described, outlining the main assets of each section and willing to easy the reading process.
\begin{itemize}
\item \textbf{Chapter 1 - Introduction} presents the motivation of the project, along with the project objectives. Moreover, the context of the student is described, in order to contextualize the development of the project. Finally, the structure of the presented document is reported.
\item \textbf{Chapter 2 - State-of-the-art} discusses a run-down of the highest levels of general development achieved up to this moment in time, along with the common methodologies used within that development.
\item \textbf{Chapter 3 - Development framework} not only puts forward the software alternatives for parallel programming along with the different techniques for measuring energy in computer systems, but also describes the main assets of the project together with the SW tools used and the underlying HW architecture. 
\item \textbf{Chapter 4 - Project description} overviews the main features of the project from a formal perspective: the requirements of the final system, the different uses cases of the system, a feasibility study of the project and the costs projection are presented.  Moreover, the requirement traceability matrix, which correlates requirements with the performed tests is also presented. 
\item \textbf{Chapter 5 - Proposal} details the system objectives. Furthermore, it presents both an overview of the Flex-MPI along with this project's contribution to the library. 
\item \textbf{Chapter 6 - Evaluation} introduces the whole validation process of the application, accompanied by the presentation and evaluation of the benchmarks used.
\item \textbf{Chapter 7 - Conclusion} describes the conclusions extracted throughout the realisation of the whole project, as well as presenting future research lines and any possible improvement. 
%hacemos referencia a los anexos en la estructura del documento?

\end{itemize}
Moreover, the following sections are presented as additional content or resources that compliment the aforementioned sections:
\begin{itemize}
\item \textbf{Acronyms:} A collection of all the acronyms used throughout this document. 
\item \textbf{User Manual:} A description of the tool usage considerations. 
\item \textbf{Installation Manual:} A guide on how to install the application itself.
%\item \textbf{EuroMPI:} Paper contribution derived from this work for the EuroMPI congress. 
%\item \textbf{Jornadas:} Paper contribution derived from this work for the Jornadas SARTECO congress. 
\end{itemize}
