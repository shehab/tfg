
/* Program which uses a happens-before edge to coordinate an access to
   variable 'shared_var' between two threads.  The h-b edge is created
   by a custom (kludgesome!) mechanism and hence we need to use
   ANNOTATES_HAPPEN_{BEFORE,AFTER} to explain to Helgrind what's going
   on (else it reports a race). */

   
#include "../libraries/do_acasW.h"
#include <pthread.h>
#include <stdio.h>
#include <assert.h>

#include "../libraries/helgrind/helgrind.h"

/* Todo: move all this do_acasW guff into a support library.  It's
   useful for multiple tests, not just this one. 

   XXX: all the do_acasW routines assume the supplied address
   is UWord (naturally) aligned. */

void atomic_incW ( UWord* w )
{
   while (1) {
      UWord old = *w;
      UWord nyu = old + 1;
      UWord ok = do_acasW( w, old, nyu );
      if (ok) break;
   };
}

#if 0

#define NNN 1000000

void* thread_fn ( void* arg )
{
  UWord* w = (UWord*)arg;
  int i;
  for (i = 0; i < NNN; i++)
     atomic_incW( w );
  return NULL;
}


int main ( void )
{
   int r;
  //ANNOTATE_HAPPENS_BEFORE(0);
  //return 0;
   UWord w = 0;
  pthread_t t1, t2;

  r= pthread_create( &t1, NULL, &thread_fn, (void*)&w );   assert(!r);
  r= pthread_create( &t2, NULL, &thread_fn, (void*)&w );   assert(!r);

  r= pthread_join( t1, NULL );   assert(!r);
  r= pthread_join( t2, NULL );   assert(!r);

  printf("result = %lu\n", w );
  return 0;
}

#endif

int shared_var = 0;  // is not raced upon


void delayXms ( int i )
{
   struct timespec ts = { 0, 1 * 1000 * 1000 };
   // We do the sleep in small pieces to have scheduling
   // events ensuring a fair switch between threads, even
   // without --fair-sched=yes. This is a.o. needed for
   // running this test under an outer helgrind or an outer
   // sgcheck.
   while (i > 0) {
      nanosleep(&ts, NULL);
      i--;
   }
}

void do_wait ( UWord* w )
{
  UWord w0 = *w;
  UWord volatile * wV = w;
  while (*wV == w0)
    delayXms(1); // small sleeps, ensuring context switches
  ANNOTATE_HAPPENS_AFTER(w);
}

void do_signal ( UWord* w )
{
  ANNOTATE_HAPPENS_BEFORE(w);
  atomic_incW(w);
}



void* thread_fn1 ( void* arg )
{
  UWord* w = (UWord*)arg;
  delayXms(500);    // ensure t2 gets to its wait first
  shared_var = 1;  // first access
  do_signal(w);    // cause h-b edge to second thread

  delayXms(500);
  return NULL;
}

void* thread_fn2 ( void* arg )
{
  UWord* w = (UWord*)arg;
  do_wait(w);      // wait for h-b edge from first thread
  shared_var = 2;  // second access

  delayXms(500);
  return NULL;
}






int main ( void )
{
   int r;
   UWord w = 0;
   pthread_t t1, t2;

   r= pthread_create( &t1, NULL, &thread_fn1, (void*)&w );   assert(!r);
   r= pthread_create( &t2, NULL, &thread_fn2, (void*)&w );   assert(!r);

   r= pthread_join( t1, NULL );   assert(!r);
   r= pthread_join( t2, NULL );   assert(!r);
   return 0;
}
