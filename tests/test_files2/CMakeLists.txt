set(CMAKE_RUNTIME_OUTPUT_DIRECTORY executables)

#Compiling

add_executable(annotate_barrier annotate_barrier.c)
target_link_libraries (annotate_barrier ${CMAKE_THREAD_LIBS_INIT})

add_executable(annotate_hb_err annotate_hb_err.c)
target_link_libraries (annotate_hb_err ${CMAKE_THREAD_LIBS_INIT})

add_executable(annotate_hb_race annotate_hb_race.c)
target_link_libraries (annotate_hb_race ${CMAKE_THREAD_LIBS_INIT})

add_executable(annotate_ignore_rw annotate_ignore_rw.c)
target_link_libraries (annotate_ignore_rw ${CMAKE_THREAD_LIBS_INIT})

add_executable(annotate_ignore_write annotate_ignore_write.c)
target_link_libraries (annotate_ignore_write ${CMAKE_THREAD_LIBS_INIT})

add_executable(annotate_publish_hg annotate_publish_hg.c)
target_link_libraries (annotate_publish_hg ${CMAKE_THREAD_LIBS_INIT})

add_executable(annotate_rwlock2 annotate_rwlock.c)
target_link_libraries (annotate_rwlock2 ${CMAKE_THREAD_LIBS_INIT})

add_executable(annotate_sem annotate_sem.c)
target_link_libraries (annotate_sem ${CMAKE_THREAD_LIBS_INIT})

add_executable(annotate_smart_pointer annotate_smart_pointer.cpp)
target_link_libraries (annotate_smart_pointer ${CMAKE_THREAD_LIBS_INIT})

add_executable(annotate_static annotate_static.cpp)
target_link_libraries (annotate_static ${CMAKE_THREAD_LIBS_INIT})

add_executable(annotate_trace_memory annotate_trace_memory.c)
target_link_libraries (annotate_trace_memory ${CMAKE_THREAD_LIBS_INIT})

add_executable(atomic_var atomic_var.c)
target_link_libraries (atomic_var ${CMAKE_THREAD_LIBS_INIT})

add_executable(boost_thread boost_thread.cpp)
target_link_libraries (boost_thread ${CMAKE_THREAD_LIBS_INIT} ${Boost_LIBRARIES})
target_link_libraries (boost_thread ${CPP_STD})
target_link_libraries (boost_thread ${CPP_LIB})

add_executable(bug-235681 bug-235681.c)
target_link_libraries (bug-235681 ${CMAKE_THREAD_LIBS_INIT})

add_executable(circular_buffer circular_buffer.c)
target_link_libraries (circular_buffer ${CMAKE_THREAD_LIBS_INIT})

add_executable(concurrent_close concurrent_close.cpp)
target_link_libraries (concurrent_close ${CMAKE_THREAD_LIBS_INIT})

add_executable(custom_alloc custom_alloc.c)
target_link_libraries (custom_alloc ${CMAKE_THREAD_LIBS_INIT})

add_executable(fp_race fp_race.c)
target_link_libraries (fp_race ${CMAKE_THREAD_LIBS_INIT})

add_executable(free_is_write2 free_is_write.c)
target_link_libraries (free_is_write2 ${CMAKE_THREAD_LIBS_INIT})

add_executable(hold_lock hold_lock.c)
target_link_libraries (hold_lock ${CMAKE_THREAD_LIBS_INIT})

add_executable(linuxthreads_det linuxthreads_det.c)
target_link_libraries (linuxthreads_det ${CMAKE_THREAD_LIBS_INIT})

add_executable(local_static local_static.cpp)
target_link_libraries (local_static ${CMAKE_THREAD_LIBS_INIT})

add_executable(matinv matinv.c)
target_link_libraries (matinv ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries (matinv ${MATH})

add_executable(memory_allocation memory_allocation.c)
target_link_libraries (memory_allocation ${CMAKE_THREAD_LIBS_INIT})

add_executable(monitor_example monitor_example.cpp)
target_link_libraries (monitor_example ${CMAKE_THREAD_LIBS_INIT})

add_executable(new_delete new_delete.cpp)
target_link_libraries (new_delete ${CMAKE_THREAD_LIBS_INIT})

add_executable(omp_prime omp_prime.c)
target_link_libraries (omp_prime ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries (omp_prime ${MATH})

add_executable(omp_printf omp_printf.c)
target_link_libraries (omp_printf ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries (omp_printf ${OpenMP_CXX_FLAGS})

add_executable(pth_barrier pth_barrier.c)
target_link_libraries (pth_barrier ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_barrier_race pth_barrier_race.c)
target_link_libraries (pth_barrier_race ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_barrier_reinit pth_barrier_reinit.c)
target_link_libraries (pth_barrier_reinit ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_barrier_thr_cr pth_barrier_thr_cr.c)
target_link_libraries (pth_barrier_thr_cr ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_broadcast pth_broadcast.c)
target_link_libraries (pth_broadcast ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_cancel_locked pth_cancel_locked.c)
target_link_libraries (pth_cancel_locked ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_cleanup_handler pth_cleanup_handler.c)
target_link_libraries (pth_cleanup_handler ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_cond_destroy_busy pth_cond_destroy_busy.c)
target_link_libraries (pth_cond_destroy_busy ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_cond_race pth_cond_race.c)
target_link_libraries (pth_cond_race ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_create_chain pth_create_chain.c)
target_link_libraries (pth_create_chain ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_create_glibc_2_0 pth_create_glibc_2_0.c)
target_link_libraries (pth_create_glibc_2_0 ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries (pth_create_glibc_2_0 ${EXTRA_LIBS})

add_executable(pth_detached pth_detached.c)
target_link_libraries (pth_detached ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_detached3 pth_detached3.c)
target_link_libraries (pth_detached3 ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_detached_sem pth_detached_sem.c)
target_link_libraries (pth_detached_sem ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_inconsistent_cond_wait pth_inconsistent_cond_wait.c)
target_link_libraries (pth_inconsistent_cond_wait ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_mutex_reinit pth_mutex_reinit.c)
target_link_libraries (pth_mutex_reinit ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_process_shared_mutex pth_process_shared_mutex.c)
target_link_libraries (pth_process_shared_mutex ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_spinlock pth_spinlock.c)
target_link_libraries (pth_spinlock ${CMAKE_THREAD_LIBS_INIT})

add_executable(pth_uninitialized_cond pth_uninitialized_cond.c)
target_link_libraries (pth_uninitialized_cond ${CMAKE_THREAD_LIBS_INIT})

add_executable(recursive_mutex recursive_mutex.c)
target_link_libraries (recursive_mutex ${CMAKE_THREAD_LIBS_INIT})

add_executable(rwlock_race rwlock_race.c)
target_link_libraries (rwlock_race ${CMAKE_THREAD_LIBS_INIT})

add_executable(rwlock_test rwlock_test.c)
target_link_libraries (rwlock_test ${CMAKE_THREAD_LIBS_INIT})

add_executable(rwlock_type_checking rwlock_type_checking.c)
target_link_libraries (rwlock_type_checking ${CMAKE_THREAD_LIBS_INIT})

add_executable(sem_as_mutex sem_as_mutex.c)
target_link_libraries (sem_as_mutex ${CMAKE_THREAD_LIBS_INIT})

add_executable(sem_open sem_open.c)
target_link_libraries (sem_open ${CMAKE_THREAD_LIBS_INIT})

add_executable(sem_wait sem_wait.cpp)
target_link_libraries (sem_wait ${CMAKE_THREAD_LIBS_INIT})

add_executable(sigalrm sigalrm.c)
target_link_libraries (sigalrm ${CMAKE_THREAD_LIBS_INIT})

add_executable(std_atomic std_atomic.cpp)
target_link_libraries (std_atomic ${CPP_STD})
target_link_libraries (std_atomic ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries (std_atomic ${CPP_LIB})

add_executable(std_list std_list.cpp)
target_link_libraries (std_list ${CPP_STD})
target_link_libraries (std_list ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries (std_list ${CPP_LIB})

add_executable(std_string std_string.cpp)
target_link_libraries (std_string ${CPP_STD})
target_link_libraries (std_string ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries (std_string ${CPP_LIB})

add_executable(std_thread std_thread.cpp)
target_link_libraries (std_thread ${CPP_STD})
target_link_libraries (std_thread ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries (std_thread ${CPP_LIB})

add_executable(std_thread2 std_thread2.cpp)
target_link_libraries (std_thread2 ${CPP_STD})
target_link_libraries (std_thread2 ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries (std_thread2 ${CPP_LIB})

add_executable(thread_name thread_name.c)
target_link_libraries (thread_name ${CMAKE_THREAD_LIBS_INIT})

add_executable(threaded-fork threaded-fork.c)
target_link_libraries (threaded-fork ${CMAKE_THREAD_LIBS_INIT})

add_executable(trylock trylock.c)
target_link_libraries (trylock ${CMAKE_THREAD_LIBS_INIT})

add_executable(tsan_unittest tsan_unittest.cpp)
target_link_libraries (tsan_unittest ${CMAKE_THREAD_LIBS_INIT})

# add_executable(unit_bitmap unit_bitmap.c)
# target_link_libraries (unit_bitmap ${CMAKE_THREAD_LIBS_INIT})

# add_executable(unit_vc unit_vc.c)
# target_link_libraries (unit_vc ${CMAKE_THREAD_LIBS_INIT})



#Helgrind Tests

add_helgrind_test(annotate_barrier_helg "executables/annotate_barrier")
add_helgrind_test(annotate_hb_err_helg "executables/annotate_hb_err")
add_helgrind_test(annotate_hb_race_helg "executables/annotate_hb_race")
add_helgrind_test(annotate_ignore_rw_helg "executables/annotate_ignore_rw")
add_helgrind_test(annotate_ignore_write_helg "executables/annotate_ignore_write")
add_helgrind_test(annotate_publish_hg_helg "executables/annotate_publish_hg")
add_helgrind_test(annotate_rwlock_helg "executables/annotate_rwlock")
add_helgrind_test(annotate_sem_helg "executables/annotate_sem")
add_helgrind_test(annotate_smart_pointer_helg "executables/annotate_smart_pointer")
add_helgrind_test(annotate_static_helg "executables/annotate_static")
add_helgrind_test(annotate_trace_memory_helg "executables/annotate_trace_memory")
add_helgrind_test(atomic_var_helg "executables/atomic_var")
add_helgrind_test(boost_thread_helg "executables/boost_thread")
add_helgrind_test(bug-235681_helg "executables/bug-235681")
add_helgrind_test(circular_buffer_helg "executables/circular_buffer")
add_helgrind_test(concurrent_close_helg "executables/concurrent_close")
add_helgrind_test(custom_alloc_helg "executables/custom_alloc")
add_helgrind_test(fp_race_helg "executables/fp_race")
add_helgrind_test(free_is_write_helg "executables/free_is_write")
add_helgrind_test(hold_lock_helg "executables/hold_lock")
add_helgrind_test(linuxthreads_det_helg "executables/linuxthreads_det")
add_helgrind_test(local_static_helg "executables/local_static")
add_helgrind_test(matinv_helg "executables/matinv")
add_helgrind_test(memory_allocation_helg "executables/memory_allocation")
add_helgrind_test(monitor_example_helg "executables/monitor_example")
add_helgrind_test(new_delete_helg "executables/new_delete")
add_helgrind_test(omp_matinv_helg "executables/omp_matinv")
add_helgrind_test(omp_prime_helg "executables/omp_prime")
add_helgrind_test(omp_printf_helg "executables/omp_printf")
add_helgrind_test(pth_barrier_helg "executables/pth_barrier")
add_helgrind_test(pth_barrier_race_helg "executables/pth_barrier_race")
add_helgrind_test(pth_barrier_thr_cr_helg "executables/pth_barrier_thr_cr")
add_helgrind_test(pth_broadcast_helg "executables/pth_broadcast")
add_helgrind_test(pth_cancel_locked_helg "executables/pth_cancel_locked")
add_helgrind_test(pth_cleanup_handler_helg "executables/pth_cleanup_handler")
add_helgrind_test(pth_cond_destroy_busy_helg "executables/pth_cond_destroy_busy")
add_helgrind_test(pth_cond_race_helg "executables/pth_cond_race")
add_helgrind_test(pth_create_chain_helg "executables/pth_create_chain")
add_helgrind_test(pth_create_glibc_2_0_helg "executables/pth_create_glibc_2_0")
add_helgrind_test(pth_detached_helg "executables/pth_detached")
add_helgrind_test(pth_detached3_helg "executables/pth_detached3")
add_helgrind_test(pth_detached_sem_helg "executables/pth_detached_sem")
add_helgrind_test(pth_inconsistent_cond_wait_helg "executables/pth_inconsistent_cond_wait")
add_helgrind_test(pth_mutex_reinit_helg "executables/pth_mutex_reinit")
add_helgrind_test(pth_process_shared_mutex_helg "executables/pth_process_shared_mutex")
add_helgrind_test(pth_spinlock_helg "executables/pth_spinlock")
add_helgrind_test(pth_uninitialized_cond_helg "executables/pth_uninitialized_cond")
add_helgrind_test(recursive_mutex_helg "executables/recursive_mutex")
add_helgrind_test(rwlock_race_helg "executables/rwlock_race")
add_helgrind_test(rwlock_test_helg "executables/pth_mutex_reinit")
add_helgrind_test(rwlock_type_checking_helg "executables/rwlock_type_checking")
add_helgrind_test(sem_as_mutex_helg "executables/sem_as_mutex")
add_helgrind_test(sem_open_helg "exuecutables/sem_open")
add_helgrind_test(sem_wait_helg "executables/sem_wait")
add_helgrind_test(sigalrm_helg "executables/sigalrm")
add_helgrind_test(std_atomic_helg "executables/std_atomic")
add_helgrind_test(std_list_helg "executables/std_list")
add_helgrind_test(std_string_helg "executables/std_string")
add_helgrind_test(std_thread_helg "executables/std_thread")
add_helgrind_test(std_thread2_helg "executables/std_thread2")
add_helgrind_test(thread_name_helg "executables/thread_name")
add_helgrind_test(threaded-fork_helg "executables/threaded-fork")
add_helgrind_test(trylock_helg "executables/trylock")
add_helgrind_test(tsan_unittest_helg "executables/tsan_unittest")
add_helgrind_test(unit_bitmap_helg "executables/unit_bitmap")
add_helgrind_test(unit_vc_helg "executables/unit_vc")


#DRD Tests

add_drd_test(annotate_barrier_drd "executables/annotate_barrier")
add_drd_test(annotate_hb_err_drd "executables/annotate_hb_err")
add_drd_test(annotate_hb_race_drd "executables/annotate_hb_race")
add_drd_test(annotate_ignore_rw_drd "executables/annotate_ignore_rw")
add_drd_test(annotate_ignore_write_drd "executables/annotate_ignore_write")
add_drd_test(annotate_publish_hg_drd "executables/annotate_publish_hg")
add_drd_test(annotate_rwlock_drd "executables/annotate_rwlock")
add_drd_test(annotate_sem_drd "executables/annotate_sem")
add_drd_test(annotate_smart_pointer_drd "executables/annotate_smart_pointer")
add_drd_test(annotate_static_drd "executables/annotate_static")
add_drd_test(annotate_trace_memory_drd "executables/annotate_trace_memory")
add_drd_test(atomic_var_drd "executables/atomic_var")
add_drd_test(boost_thread_drd "executables/boost_thread")
add_drd_test(bug-235681_drd "executables/bug-235681")
add_drd_test(circular_buffer_drd "executables/circular_buffer")
add_drd_test(concurrent_close_drd "executables/concurrent_close")
add_drd_test(custom_alloc_drd "executables/custom_alloc")
add_drd_test(fp_race_drd "executables/fp_race")
add_drd_test(free_is_write_drd "executables/free_is_write")
add_drd_test(hold_lock_drd "executables/hold_lock")
add_drd_test(linuxthreads_det_drd "executables/linuxthreads_det")
add_drd_test(local_static_drd "executables/local_static")
add_drd_test(matinv_drd "executables/matinv")
add_drd_test(memory_allocation_drd "executables/memory_allocation")
add_drd_test(monitor_example_drd "executables/monitor_example")
add_drd_test(new_delete_drd "executables/new_delete")
add_drd_test(omp_matinv_drd "executables/omp_matinv")
add_drd_test(omp_prime_drd "executables/omp_prime")
add_drd_test(omp_printf_drd "executables/omp_printf")
add_drd_test(pth_barrier_drd "executables/pth_barrier")
add_drd_test(pth_barrier_race_drd "executables/pth_barrier_race")
add_drd_test(pth_barrier_thr_cr_drd "executables/pth_barrier_thr_cr")
add_drd_test(pth_broadcast_drd "executables/pth_broadcast")
add_drd_test(pth_cancel_locked_drd "executables/pth_cancel_locked")
add_drd_test(pth_cleanup_handler_drd "executables/pth_cleanup_handler")
add_drd_test(pth_cond_destroy_busy_drd "executables/pth_cond_destroy_busy")
add_drd_test(pth_cond_race_drd "executables/pth_cond_race")
add_drd_test(pth_create_chain_drd "executables/pth_create_chain")
add_drd_test(pth_create_glibc_2_0_drd "executables/pth_create_glibc_2_0")
add_drd_test(pth_detached_drd "executables/pth_detached")
add_drd_test(pth_detached3_drd "executables/pth_detached3")
add_drd_test(pth_detached_sem_drd "executables/pth_detached_sem")
add_drd_test(pth_inconsistent_cond_wait_drd "executables/pth_inconsistent_cond_wait")
add_drd_test(pth_mutex_reinit_drd "executables/pth_mutex_reinit")
add_drd_test(pth_process_shared_mutex_drd "executables/pth_process_shared_mutex")
add_drd_test(pth_spinlock_drd "executables/pth_spinlock")
add_drd_test(pth_uninitialized_cond_drd "executables/pth_uninitialized_cond")
add_drd_test(recursive_mutex_drd "executables/recursive_mutex")
add_drd_test(rwlock_race_drd "executables/rwlock_race")
add_drd_test(rwlock_test_drd "executables/pth_mutex_reinit")
add_drd_test(rwlock_type_checking_drd "executables/rwlock_type_checking")
add_drd_test(sem_as_mutex_drd "executables/sem_as_mutex")
add_drd_test(sem_open_drd "exuecutables/sem_open")
add_drd_test(sem_wait_drd "executables/sem_wait")
add_drd_test(sigalrm_drd "executables/sigalrm")
add_drd_test(std_atomic_drd "executables/std_atomic")
add_drd_test(std_list_drd "executables/std_list")
add_drd_test(std_string_drd "executables/std_string")
add_drd_test(std_thread_drd "executables/std_thread")
add_drd_test(std_thread2_drd "executables/std_thread2")
add_drd_test(thread_name_drd "executables/thread_name")
add_drd_test(threaded-fork_drd "executables/threaded-fork")
add_drd_test(trylock_drd "executables/trylock")
add_drd_test(tsan_unittest_drd "executables/tsan_unittest")
add_drd_test(unit_bitmap_drd "executables/unit_bitmap")
add_drd_test(unit_vc_drd "executables/unit_vc")