#if defined(__powerpc64__) || defined(M_PPC)
    #define VGA_ppc64be
    #define VGA_ppc64le
#endif

#if defined(__powerpc__) || defined(M_PPC)
    #define VGA_ppc32
#endif

#if defined(__amd64__) || defined(_M_AMD64)
    #define VGA_amd64    
#endif

#if defined(__i386__) || defined(_M_I86) || defined(_M_IX86)
    #define VGA_x86
#endif

#if defined(__arm__)
    #define VGA_arm
#endif

#if defined(__aarch64__)
    #define VGA_arm64
#endif

#if defined(__s390x__)
    #define VGA_s390x
#endif

#if defined(__mips__)
    #if UINTPTR_MAX == 0xffffffff
        #define VGA_mips32
    #elif UINTPTR_MAX == 0xffffffffffffffff
        #define VGA_mips64
    #endif
#endif

#if defined(_M_ARM)
    #if UINTPTR_MAX == 0xffffffff
        #define VGA_arm
    #elif UINTPTR_MAX == 0xffffffffffffffff
        #define VGA_arm64
    #endif
#endif
    