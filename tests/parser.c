#include <stdio.h>
#include <stdlib.h>

//values
int current_char;
int current_test;

//buffers
char file_name[3];

//flags
char done;
char new_line;

FILE *fp_in; 
FILE *fp_out;

void get_current_test(){
    int digit_1;
    int digit_2;
    char comp = 0;

    current_char = fgetc(fp_in);
    if(current_char >= 49 && current_char <= 57){
        comp = 1;
        digit_1 = current_char - 48;
    }
    current_char = fgetc(fp_in);
    if(current_char >= 48 && current_char <= 57){
        comp = 2;
        digit_2 = current_char - 48;
    }
    if(comp == 1){
        current_test = digit_1;
        printf("%d\n", current_test);
    } else if(comp == 2){
        comp = 0;
        current_test = digit_1*10 + digit_2;
        printf("%d\n", current_test);
    }
    digit_1 = 0;
    digit_2 = 0;
}

void write_line(){
    // fprintf(fp_out, file_name);
    do{
        current_char = fgetc(fp_in);
        // fprintf(fp_out, current_char);
    }while(current_char != '\n' && current_char != EOF);

    if(current_char == EOF){
        done = 1;
    }
}


int main(){
    done = 0;
    fp_in = fopen("results/helgrind_test.log", "r");
    while(!done){
        get_current_test();
        sprintf(file_name, "results/test_%d", current_test);
        printf("%s\n", file_name);
        write_line();
    }

    return 0;
}