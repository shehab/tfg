clang -fsanitize=thread -g -01 annotate_hbefore.c -o annotate_hbefore
clang -fsanitize=thread -g -01 annotate_rwlock.c -o annotate_rwlock
clang -fsanitize=thread -g -01 bar_bad.c -o bar_bad
clang -fsanitize=thread -g -01 bar_trivial.c -o bar_trivial
clang -fsanitize=thread -g -01 cond_init_destroy.c -o cond_init_destroy
clang -fsanitize=thread -g -01 cond_timedwait_invalid.c -o cond_timedwait_invalid
clang -fsanitize=thread -g -01 cond_timedwait_test.c -o cond_timedwait_test
clang -fsanitize=thread -g -01 free_is_write.c -o free_is_write
clang -fsanitize=thread -g -01 hg01_all_ok.c -o hg01_all_ok
clang -fsanitize=thread -g -01 hg02_deadlock.c -o hg02_deadlock
clang -fsanitize=thread -g -01 hg03_inherit.c -o hg03_inherit
clang -fsanitize=thread -g -01 hg04_race.c -o hg04_race
clang -fsanitize=thread -g -01 hg05_race2.c -o hg05_race2
clang -fsanitize=thread -g -01 hg06_readshared.c -o hg06_readshared
clang -fsanitize=thread -g -01 locked_vs_unlocked1.c -o locked_vs_unlocked1
clang -fsanitize=thread -g -01 locked_vs_unlocked2.c -o locked_vs_unlocked2
clang -fsanitize=thread -g -01 locked_vs_unlocked3.c -o locked_vs_unlocked3
clang -fsanitize=thread -g -01 pth_destroy_cond.c -o pth_destroy_cond
clang -fsanitize=thread -g -01 t2t.c -o t2t
clang -fsanitize=thread -g -01 tc01_simple_race.c -o tc01_simple_race
clang -fsanitize=thread -g -01 tc02_simple_tls.c -o tc02_simple_tls
clang -fsanitize=thread -g -01 tc03_re_excl.c -o tc03_re_excl
clang -fsanitize=thread -g -01 tc04_free_lock.c -o tc04_free_lock
clang -fsanitize=thread -g -01 tc05_simple_race.c -o tc05_simple_race
clang -fsanitize=thread -g -01 tc06_two_races.c -o tc06_two_races
clang -fsanitize=thread -g -01 tc07_hbl1.c -o tc07_hbl1
clang -fsanitize=thread -g -01 tc08_hbl2.c -o tc08_hbl2
clang -fsanitize=thread -g -01 tc09_bad_unlock.c -o tc09_bad_unlock
clang -fsanitize=thread -g -01 tc10_rec_lock.c -o tc10_rec_lock
clang -fsanitize=thread -g -01 tc11_XCHG.c -o tc11_XCHG
clang -fsanitize=thread -g -01 tc12_rwl_trivial.c -o tc12_rwl_trivial
clang -fsanitize=thread -g -01 tc13_laog1.c -o tc13_laog1
clang -fsanitize=thread -g -01 tc14_laog_dinphils.c -o tc14_laog_dinphils
clang -fsanitize=thread -g -01 tc15_laog_lockdel.c -o tc15_laog_lockdel
clang -fsanitize=thread -g -01 tc16_byterace.c -o tc16_byterace
clang -fsanitize=thread -g -01 tc17_sembar.c -o tc17_sembar
clang -fsanitize=thread -g -01 tc18_semabuse.c -o tc18_semabuse
clang -fsanitize=thread -g -01 tc19_shadowmem.c -o tc19_shadowmem
clang -fsanitize=thread -g -01 tc20_verifywrap.c -o tc20_verifywrap
clang -fsanitize=thread -g -01 tc21_pthonce.c -o tc21_pthonce
clang -fsanitize=thread -g -01 tc22_exit_w_lock.c -o tc22_exit_w_lock
clang -fsanitize=thread -g -01 tc23_bogus_condwait.c -o tc23_bogus_condwait
clang -fsanitize=thread -g -01 tc24_nonzero_sem.c -o tc24_nonzero_sem
clang -fsanitize=thread -g -01 tls_threads.c -o tls_threads


clang -fsanitize=thread -g -01 annotate_barrier.c -o annotate_barrier
clang -fsanitize=thread -g -01 annotate_hb_err.c -o annotate_hb_err
clang -fsanitize=thread -g -01 annotate_hb_race.c -o annotate_hb_race
clang -fsanitize=thread -g -01 annotate_ignore_rw.c -o annotate_ignore_rw
clang -fsanitize=thread -g -01 annotate_ignore_write.c -o annotate_ignore_write
clang -fsanitize=thread -g -01 annotate_publish_hg.c -o annotate_publish_hg
clang -fsanitize=thread -g -01 annotate_rwlock2.c -o annotate_rwlock2
clang -fsanitize=thread -g -01 annotate_sem.c -o annotate_sem
clang -std=c++11  -fsanitize=thread -g -01 annotate_smart_pointer.cpp -o annotate_smart_pointer
clang -std=c++11  -fsanitize=thread -g -01 annotate_static.cpp -o annotate_static
clang -fsanitize=thread -g -01 annotate_trace_memory.c -o annotate_trace_memory
clang -fsanitize=thread -g -01 atomic_var.c -o atomic_var
clang -std=c++11  -fsanitize=thread -g -01 boost_thread.cpp -o boost_thread
clang -fsanitize=thread -g -01 bug-235681.c -o bug-235681
clang -fsanitize=thread -g -01 circular_buffer.c -o circular_buffer
clang -fsanitize=thread -g -01 concurrent_close.c -o concurrent_close
clang -fsanitize=thread -g -01 custom_alloc.c -o custom_alloc
clang -fsanitize=thread -g -01 fp_race.c -o fp_race
clang -fsanitize=thread -g -01 free_is_write2.c -o free_is_write2
clang -fsanitize=thread -g -01 hold_lock.c -o hold_lock
clang -fsanitize=thread -g -01 linuxthreads_det.c -o linuxthreads_det
clang -std=c++11  -fsanitize=thread -g -01 local_static.cpp -o local_static
clang -lm -fsanitize=thread -g -01 matinv.c -o matinv
clang -fsanitize=thread -g -01 memory_allocation.c -o memory_allocation
clang -std=c++11  -fsanitize=thread -g -01 monitor_example.cpp -o monitor_example
clang -std=c++11  -fsanitize=thread -g -01 new_delete.cpp -o new_delete
clang -fsanitize=thread -g -01 omp_prime.c -o omp_prime
clang -fsanitize=thread -g -01 omp_printf.c -o omp_printf
clang -fsanitize=thread -g -01 pth_barrier.c -o pth_barrier 
clang -fsanitize=thread -g -01 pth_barrier_race.c -o pth_barrier_race
clang -fsanitize=thread -g -01 pth_barrier_reinit.c -o pth_barrier_reinit
clang -fsanitize=thread -g -01 pth_barrier_thr_cr.c -o pth_barrier_thr_cr
clang -fsanitize=thread -g -01 pth_broadcast.c -o pth_broadcast
clang -fsanitize=thread -g -01 pth_cancel_locked.c -o pth_cancel_locked
clang -fsanitize=thread -g -01 pth_cleanup_handler.c -o pth_cleanup_handler
clang -fsanitize=thread -g -01 pth_cond_destroy_busy.c -o pth_cond_destroy_busy
clang -fsanitize=thread -g -01 pth_cond_race.c -o pth_cond_race
clang -fsanitize=thread -g -01 pth_create_chain.c -o pth_create_chain
clang -fsanitize=thread -g -01 pth_create_glibc_2_0.c -o pth_create_glibc_2_0
clang -fsanitize=thread -g -01 pth_detached.c -o pth_detached
clang -fsanitize=thread -g -01 pth_detached2.c -o pth_detached2
clang -fsanitize=thread -g -01 pth_detached_sem.c -o pth_detached_sem
clang -fsanitize=thread -g -01 pth_inconsistent_cond_wait.c -o pth_inconsistent_cond_wait
clang -fsanitize=thread -g -01 pth_mutex_reinit.c -o pth_mutex_reinit
clang -fsanitize=thread -g -01 pth_process_shared_mutex.c -o pth_process_shared_mutex
clang -fsanitize=thread -g -01 pth_spinlock.c -o pth_spinlock
clang -fsanitize=thread -g -01 pth_uninitialized_cond.c -o pth_uninitialized_cond
clang -fsanitize=thread -g -01 recursive_mutex.c -o recursive_mutex
clang -fsanitize=thread -g -01 rwlock_race.c -o rwlock_race
clang -fsanitize=thread -g -01 rwlock_test.c -o rwlock_test
clang -fsanitize=thread -g -01 rwlock_type_checking.c -o rwlock_type_checking
clang -fsanitize=thread -g -01 sem_as_mutex.c -o sem_as_mutex
clang -fsanitize=thread -g -01 sem_open.c -o sem_open
clang -fsanitize=thread -g -01 sem_wait.c sem_wait-o
clang -fsanitize=thread -g -01 sigalrm.c -o sigalrm
clang -std=c++11  -fsanitize=thread -g -01 std_atomic.cpp -o std_atomic
clang -std=c++11  -fsanitize=thread -g -01 std_list.cpp -o std_list
clang -std=c++11  -fsanitize=thread -g -01 std_string.cpp -o std_string
clang -std=c++11  -fsanitize=thread -g -01 std_thread.cpp -o std_thread
clang -std=c++11  -fsanitize=thread -g -01 std_thread2.cpp -o std_thread2
clang -fsanitize=thread -g -01 thread_name.c -o thread_name
clang -fsanitize=thread -g -01 threaded-fork.c -o threaded-fork
clang -fsanitize=thread -g -01 trylock.c -o trylock
clang -std=c++11  -fsanitize=thread -g -01 tsan_unittest.cpp -o tsan_unittest
clang -fsanitize=thread -g -01 unit_bitmap.c -o unit_bitmap
clang -fsanitize=thread -g -01 unit_vc.c -o unit_vc