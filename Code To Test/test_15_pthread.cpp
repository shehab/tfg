#include <string>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <thread>

using namespace std;

pthread_t thread_1;
pthread_barrier_t bar;

void *barrier_holder(void*){
    pthread_barrier_wait(&bar);
    pthread_exit(NULL);
}

int main(){
    //Testing Feature 15
    //Waiting on an uninitialised pthread barrier.
    pthread_create(&thread_1, NULL, barrier_holder, NULL);
    pthread_join(thread_1, NULL);
}