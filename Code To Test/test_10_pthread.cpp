#include <string>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <thread>

using namespace std;

/*
 * Uncomment the sleeps for the second error   
 */

pthread_mutex_t mutex, mut;
pthread_cond_t cond_1, cond_2;
pthread_t thread_1, thread_2;

void *waiter(void*){
    // sleep(1);
    pthread_cond_wait(&cond_1, &mutex);
    pthread_cond_wait(&cond_2, &mut);
    pthread_exit(NULL);
}
void *signaler(void*){
    pthread_mutex_lock(&mutex);
    // sleep(2);
    pthread_cond_signal(&cond_1);
    pthread_mutex_unlock(&mutex);   //If mutex is not unlocked, the program does not finish (helgrind)
    pthread_exit(NULL);
}

int main(){
    //Testing Feature 10:
    //Calling pthread_cond_wait with 
    //a not-locked mutex, an invalid mutex, 
    //or one locked by a different thread.
    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&cond_1, NULL);
    pthread_cond_init(&cond_2, NULL);
    pthread_create(&thread_1, NULL, waiter, NULL);
    pthread_create(&thread_2, NULL, signaler, NULL);
    pthread_join(thread_1, NULL);
    pthread_join(thread_2, NULL);
}