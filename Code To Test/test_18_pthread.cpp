#include <string>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <thread>

using namespace std;

pthread_t thread_1, thread_2;
pthread_rwlock_t rw_lock;

void *unlocker(void*){
    sleep(1);
    pthread_rwlock_unlock(&rw_lock);
    pthread_exit(0);
}

void *locker(void*){
    pthread_rwlock_wrlock(&rw_lock);
    sleep(2);
    pthread_exit(0);

}

int main(){
    pthread_create(&thread_1, NULL, locker, NULL);
    pthread_create(&thread_2, NULL, unlocker, NULL);
    pthread_join(thread_1, NULL);
    pthread_join(thread_2, NULL);
    return 0;
}