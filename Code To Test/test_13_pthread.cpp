#include <string>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <thread>

using namespace std;

pthread_t thread_1, thread_2, thread_3;
pthread_barrier_t bar;

void *barrier_holder(void*){
    pthread_barrier_wait(&bar);
    pthread_exit(NULL);
}
void *reinitializor(void*){
    pthread_barrier_init(&bar, NULL, 1);
    pthread_exit(NULL);
}

int main(){
    //Testing Feature 13:
    //Initialisation of a pthread barrier
    //on which threads are still waiting.
    pthread_barrier_init(&bar, NULL, 2);
    pthread_create(&thread_1, NULL, barrier_holder, NULL);
    pthread_create(&thread_2, NULL, reinitializor, NULL);
    sleep(2);
    pthread_create(&thread_3, NULL, barrier_holder, NULL);

    pthread_join(thread_1, NULL);
    pthread_join(thread_2, NULL);
    pthread_join(thread_3, NULL);
}