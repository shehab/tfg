#include <string>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <thread>

using namespace std;

pthread_mutex_t mutex;
pthread_t thread_1, thread_2;

void *mutex_locker(void *){
    pthread_mutex_lock(&mutex);
    pthread_exit(NULL);
}

void *mutex_unlocker(void *){
    pthread_mutex_unlock(&mutex);
    pthread_exit(NULL);
}

int main(){
    // Testing Feature 3:
    // Unlocking a mutex heald by another thread
    pthread_create(&thread_1, NULL, mutex_locker, NULL);
    sleep(2);
    pthread_create(&thread_2, NULL, mutex_unlocker, NULL);

    pthread_join(thread_1, NULL);
    pthread_join(thread_2, NULL);
}