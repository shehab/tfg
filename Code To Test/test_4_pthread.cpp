#include <string>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <thread>

using namespace std;

pthread_mutex_t mut_1, mut_2;

int main(){

    pthread_mutex_destroy(&mut_1);

    pthread_mutex_lock(&mut_2);
    pthread_mutex_destroy(&mut_2);

}