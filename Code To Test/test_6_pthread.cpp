#include <string>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <thread>

using namespace std;

pthread_mutex_t mut_1, mut_2;

int main(){
    pthread_mutex_lock(&mut_1);
    pthread_mutex_destroy(&mut_1);
    pthread_mutex_lock(&mut_2);

    void *lock_mem = malloc(sizeof(pthread_mutex_t));
    memcpy(lock_mem, &mut_2, sizeof(pthread_mutex_t));
    pthread_mutex_lock((pthread_mutex_t *)lock_mem);
    free(lock_mem);
    pthread_mutex_unlock(&mut_1);
}