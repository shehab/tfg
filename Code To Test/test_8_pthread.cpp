#include <string>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <thread>

using namespace std;

pthread_mutex_t mutex;
pthread_t thread_1;
// const pthread_attr_t attr;

void *exit_holding_lock(void *){
    pthread_exit(0);
} 

int main(){
    pthread_create(&thread_1, NULL, exit_holding_lock, NULL);\
    pthread_join(thread_1, NULL);
}