#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>

using namespace std;

pthread_mutex_t mutex;

int main(){
    // Testing Feature 1:
    // Unlocking an invalid mutex
    pthread_mutex_unlock(&mutex);
    return 0;

}