#include <string>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <thread>

using namespace std;

pthread_rwlock_t rw_lock_1, rw_lock_2;

int main(){
    pthread_rwlock_wrlock(&rw_lock_1);
    pthread_rwlock_wrlock(&rw_lock_1);
    pthread_rwlock_rdlock(&rw_lock_2);
    pthread_rwlock_rdlock(&rw_lock_2);
    return 0;

}