#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>

using namespace std;

pthread_mutex_t mutex;

int main(){
    // Testing Feature 2:
    // Unlocking a mutex that is unlocked
    pthread_mutex_lock(&mutex);
    pthread_mutex_unlock(&mutex);
    pthread_mutex_unlock(&mutex); 
    return 0;
}