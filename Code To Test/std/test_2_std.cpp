#include <string>
#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

mutex mut;

int main(){
    // Testing Feature 2:
    // Unlocking a mutex that is unlocked
    mut.lock();
    mut.unlock();
    mut.unlock(); 
    return 0;
}