#include <string>
#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

mutex mut;

void mutex_locker(){
    mut.lock();
}

void mutex_unlocker(){
    mut.unlock();
}

int main(){
    // Testing Feature 3:
    // Unlocking a mutex heald by another thread
    thread thread_1(mutex_locker);
    // this_thread::sleep_for(chrono::milliseconds(2));
    thread thread_2(mutex_unlocker);

    thread_1.join();
    thread_2.join();
}