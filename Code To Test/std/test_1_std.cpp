#include <string>
#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

mutex mut;

int main(){
    // Testing Feature 1:
    // Unlocking an invalid mutex
    mut.unlock();
    return 0;
}