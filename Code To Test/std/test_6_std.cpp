#include <string>
#include <iostream>
#include <thread>

#include <string.h>
#include <mutex>

using namespace std;

mutex mut_1, mut_2;

int main(){
    mut_1.lock();
    // pthread_mutex_destroy(&mut_1);
    mut_2.lock();

    void *lock_mem = malloc(sizeof(mutex));
    memcpy(lock_mem, &mut_2, sizeof(pthread_mutex_t));
    (mutex)lock_mem.lock();
    free(lock_mem);
    return 0;
}