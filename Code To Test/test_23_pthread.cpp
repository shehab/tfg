#include <string>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <thread>
#include <semaphore.h>

using namespace std;

sem_t sem;

int main(){
    sem_init(&sem, 0, 10);
    sem_wait(&sem);
    sem_destroy(&sem);
    return 0;
}