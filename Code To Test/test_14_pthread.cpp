#include <string>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <thread>

using namespace std;

pthread_t thread_1;
pthread_barrier_t bar_1, bar_2;

void *barrier_holder(void*){
    pthread_barrier_wait(&bar_1);
    pthread_exit(NULL);
}
void uninit_barrier_destructor(){
    pthread_barrier_destroy(&bar_1);
}

int main(){
    //Testing Feature 14:
    //Destruction of a pthread barrier object
    //which was never initialised, or on which 
    //threads are still waiting.

    pthread_barrier_destroy(&bar_2);

    pthread_barrier_init(&bar_1, NULL, 2);
    pthread_create(&thread_1, NULL, barrier_holder, NULL);
    uninit_barrier_destructor();

    pthread_join(thread_1, NULL);
}