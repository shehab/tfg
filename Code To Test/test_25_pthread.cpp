#include <string>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <thread>

using namespace std;

pthread_t thread_1, thread_2;

void *empty_func(void*){
    sleep(2);
    pthread_exit(NULL);
}

int main(){
    pthread_create(&thread_1, NULL, empty_func, NULL);

    pthread_cancel(thread_2);
    pthread_join(thread_2, NULL);
    return 0;
}