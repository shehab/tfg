#include <string>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <thread>

using namespace std;

pthread_t thread_1, thread_2;
pthread_cond_t cond_1, cond_2;
pthread_mutex_t mutex_1, mutex_2;

// void *waiter(void*){
//     pthread_mutex_lock(&mutex_1);
//     printf("about to wait\n");
//     pthread_cond_wait(&cond_1, &mutex_1);
//     printf("done waiting\n");
//     pthread_mutex_unlock(&mutex_1);
//     pthread_exit(NULL);
// }
// void *signaler(void*){
//     pthread_mutex_lock(&mutex_1);
//     printf("signaler about to signal\n");
//     pthread_cond_wait(&cond_2, &mutex_1);
//     pthread_mutex_unlock(&mutex_1);
//     pthread_cond_signal(&cond_1);
//     pthread_exit(NULL);
// }

void *waiter(void*){
    pthread_mutex_lock(&mutex_1);
    printf("about to wait\n");
    pthread_cond_wait(&cond_1, &mutex_1);
    printf("done waiting\n");
    pthread_mutex_unlock(&mutex_1);
    pthread_exit(NULL);
}
void *signaler(void*){
    pthread_mutex_lock(&mutex_1);
    printf("signaler about to signal\n");
    pthread_cond_wait(&cond_1, &mutex_2);
    pthread_mutex_unlock(&mutex_1);
    pthread_cond_signal(&cond_1);
    pthread_exit(NULL);
}


int main(){
    pthread_create(&thread_1, NULL, waiter, NULL);
    pthread_create(&thread_2, NULL, signaler, NULL);
    pthread_join(thread_1, NULL);
    pthread_join(thread_2, NULL);
    return 0;
}