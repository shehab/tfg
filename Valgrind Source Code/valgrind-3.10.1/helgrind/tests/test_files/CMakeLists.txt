set(CMAKE_RUNTIME_OUTPUT_DIRECTORY executables)

MESSAGE("${CTEST_MEMORYCHECK_COMMAND_OPTIONS}")

function(add_helgrind_test name binary)
  set(threadcheck_command "${CTEST_MEMORYCHECK_COMMAND} ${CTEST_MEMORYCHECK_COMMAND_OPTIONS}")
  separate_arguments(threadcheck_command)
  add_test(threadcheck_${name} ${threadcheck_command} ./${binary} ${ARGN})
endfunction(add_helgrind_test)

add_executable(annotate_hbefore annotate_hbefore.c)
target_link_libraries (annotate_hbefore do_acasW)
target_link_libraries (annotate_hbefore ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(annotate_hbefore "executables/annotate_hbefore")

add_executable(annotate_rwlock annotate_rwlock.c)
target_link_libraries (annotate_rwlock ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(annotate_rwlock "executables/annotate_rwlock")

add_executable(bar_bad bar_bad.c)
target_link_libraries (bar_bad ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(bar_bad "executables/bar_bad")

add_executable(bar_trivial bar_trivial.c)
target_link_libraries (bar_trivial ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(bar_trivial "executables/bar_trivial")

add_executable(cond_init_destroy cond_init_destroy.c)
target_link_libraries (cond_init_destroy ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(cond_init_destroy "executables/cond_init_destroy")

add_executable(cond_timedwait_invalid cond_timedwait_invalid.c)
target_link_libraries (cond_timedwait_invalid ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(cond_timedwait_invalid "executables/cond_timedwait_invalid")

add_executable(cond_timedwait_test cond_timedwait_test.c)
target_link_libraries (cond_timedwait_test ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(cond_timedwait_test "executables/cond_timedwait_test")

add_executable(free_is_write free_is_write.c)
target_link_libraries (free_is_write ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(free_is_write "executables/free_is_write")

add_executable(hg01_all_ok hg01_all_ok.c)
target_link_libraries (hg01_all_ok ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(hg01_all_ok "executables/hg01_all_ok")

add_executable(hg02_deadlock hg02_deadlock.c)
target_link_libraries (hg02_deadlock ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(hg02_deadlock "executables/hg02_deadlock")

add_executable(hg03_inherit hg03_inherit.c)
target_link_libraries (hg03_inherit ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(hg03_inherit "executables/hg03_inherit")

add_executable(hg04_race hg04_race.c)
target_link_libraries (hg04_race ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(hg04_race "executables/hg04_race")

add_executable(hg05_race2 hg05_race2.c)
target_link_libraries (hg05_race2 ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(hg05_race2 "executables/hg05_race2")

add_executable(hg06_readshared hg06_readshared.c)
target_link_libraries (hg06_readshared ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(hg06_readshared "executables/hg06_readshared")

add_executable(locked_vs_unlocked1 locked_vs_unlocked1.c)
target_link_libraries (locked_vs_unlocked1 ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(locked_vs_unlocked1 "executables/locked_vs_unlocked1")

add_executable(locked_vs_unlocked2 locked_vs_unlocked2.c)
target_link_libraries (locked_vs_unlocked2 ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(locked_vs_unlocked2 "executables/locked_vs_unlocked2")

add_executable(locked_vs_unlocked3 locked_vs_unlocked3.c)
target_link_libraries (locked_vs_unlocked3 ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(locked_vs_unlocked3 "executables/locked_vs_unlocked3")

add_executable(pth_destroy_cond pth_destroy_cond.c)
target_link_libraries (pth_destroy_cond ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(pth_destroy_cond "executables/pth_destroy_cond")

add_executable(t2t t2t.c)
target_link_libraries (t2t ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(t2t "executables/t2t")

add_executable(tc01_simple_race tc01_simple_race.c)
target_link_libraries (tc01_simple_race ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc01_simple_race "executables/tc01_simple_race")

add_executable(tc02_simple_tls tc02_simple_tls.c)
target_link_libraries (tc02_simple_tls ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc02_simple_tls "executables/tc02_simple_tls")

add_executable(tc03_re_excl tc03_re_excl.c)
target_link_libraries (tc03_re_excl ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc03_re_excl "executables/tc03_re_excl")

add_executable(tc04_free_lock tc04_free_lock.c)
target_link_libraries (tc04_free_lock ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc04_free_lock "executables/tc04_free_lock")

add_executable(tc05_simple_race tc05_simple_race.c)
target_link_libraries (tc05_simple_race ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc05_simple_race "executables/tc05_simple_race")

add_executable(tc06_two_races tc06_two_races.c)
target_link_libraries (tc06_two_races ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc06_two_races "executables/tc06_two_races")

add_executable(tc07_hbl1 tc07_hbl1.c)
target_link_libraries (tc07_hbl1 ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc07_hbl1 "executables/tc07_hbl1")

add_executable(tc08_hbl2 tc08_hbl2.c)
target_link_libraries (tc08_hbl2 ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc08_hbl2 "executables/tc08_hbl2")

add_executable(tc09_bad_unlock tc09_bad_unlock.c)
target_link_libraries (tc09_bad_unlock ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc09_bad_unlock "executables/tc09_bad_unlock")

add_executable(tc10_rec_lock tc10_rec_lock.c)
target_link_libraries (tc10_rec_lock ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc10_rec_lock "executables/tc10_rec_lock")

add_executable(tc11_XCHG tc11_XCHG.c)
target_link_libraries (tc11_XCHG ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc11_XCHG "executables/tc11_XCHG")

add_executable(tc12_rwl_trivial tc12_rwl_trivial.c)
target_link_libraries (tc12_rwl_trivial ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc12_rwl_trivial "executables/tc12_rwl_trivial")

add_executable(tc13_laog1 tc13_laog1.c)
target_link_libraries (tc13_laog1 ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc13_laog1 "executables/tc13_laog1")

add_executable(tc14_laog_dinphils tc14_laog_dinphils.c)
target_link_libraries (tc14_laog_dinphils ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc14_laog_dinphils "executables/tc14_laog_dinphils")

add_executable(tc15_laog_lockdel tc15_laog_lockdel.c)
target_link_libraries (tc15_laog_lockdel ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc15_laog_lockdel "executables/tc15_laog_lockdel")

add_executable(tc16_byterace tc16_byterace.c)
target_link_libraries (tc16_byterace ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc16_byterace "executables/tc16_byterace")

add_executable(tc17_sembar tc17_sembar.c)
target_link_libraries (tc17_sembar ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc17_sembar "executables/tc17_sembar")

add_executable(tc18_semabuse tc18_semabuse.c)
target_link_libraries (tc18_semabuse ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc18_semabuse "executables/tc18_semabuse")

add_executable(tc19_shadowmem tc19_shadowmem.c)
target_link_libraries (tc19_shadowmem ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc19_shadowmem "executables/tc19_shadowmem")

add_executable(tc20_verifywrap tc20_verifywrap.c)
target_link_libraries (tc20_verifywrap ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc20_verifywrap "executables/tc20_verifywrap")

add_executable(tc21_pthonce tc21_pthonce.c)
target_link_libraries (tc21_pthonce ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc21_pthonce "executables/tc21_pthonce")

add_executable(tc22_exit_w_lock tc22_exit_w_lock.c)
target_link_libraries (tc22_exit_w_lock ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc22_exit_w_lock "executables/tc22_exit_w_lock")

add_executable(tc23_bogus_condwait tc23_bogus_condwait.c)
target_link_libraries (tc23_bogus_condwait ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc23_bogus_condwait "executables/tc23_bogus_condwait")

add_executable(tc24_nonzero_sem tc24_nonzero_sem.c)
target_link_libraries (tc24_nonzero_sem ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tc24_nonzero_sem "executables/tc24_nonzero_sem")

add_executable(tls_threads tls_threads.c)
target_link_libraries (tls_threads ${CMAKE_THREAD_LIBS_INIT})
add_helgrind_test(tls_threads "executables/tls_threads")
    